import Vue from 'vue'
import store from '@/store'
import App from './App.vue'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import 'es6-promise/auto'

Vue.config.productionTip = false
Vue.use(Antd);

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
