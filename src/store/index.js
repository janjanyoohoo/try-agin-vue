
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        count: 0
    },
    actions:{
        increment (context) {
            context.commit("increment")
        }
    },
    mutations: {
        increment (state) {
            state.count++
        }
    }
})